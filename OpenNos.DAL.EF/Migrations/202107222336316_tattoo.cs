﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tattoo : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.CharacterSkill", "TattooBuff");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CharacterSkill", "TattooBuff", c => c.Int(nullable: false));
        }
    }
}
