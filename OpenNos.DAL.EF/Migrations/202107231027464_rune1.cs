﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rune1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RuneEffect",
                c => new
                    {
                        RuneEffectId = c.Int(nullable: false, identity: true),
                        EquipmentSerialId = c.Guid(nullable: false),
                        Type = c.Byte(nullable: false),
                        SubType = c.Byte(nullable: false),
                        FirstData = c.Int(nullable: false),
                        SecondData = c.Int(nullable: false),
                        ThirdData = c.Int(nullable: false),
                        IsPower = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.RuneEffectId);
            
            AddColumn("dbo.ItemInstance", "RuneAmount", c => c.Byte(nullable: false));
            AddColumn("dbo.ItemInstance", "IsBreaked", c => c.Boolean(nullable: false));
            DropColumn("dbo.ItemInstance", "RuneUpgrade");
            DropColumn("dbo.ItemInstance", "RuneBroke");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ItemInstance", "RuneBroke", c => c.Boolean(nullable: false));
            AddColumn("dbo.ItemInstance", "RuneUpgrade", c => c.Byte(nullable: false));
            DropColumn("dbo.ItemInstance", "IsBreaked");
            DropColumn("dbo.ItemInstance", "RuneAmount");
            DropTable("dbo.RuneEffect");
        }
    }
}
